# To attach the IAM role to the EC2 instance
# Firstly, create an instance profile and attach the IAM role
# Lastly, attach the instance profile to the EC2 instance

# create ec2 instance profile
resource "aws_iam_instance_profile" "ec2_profile" {
  name = "var.profile"
  role = "${aws_iam_role.execrise_iam_role.name}"
}

# create the ec2 instance with assumed region: us-east-1
resource "aws_instance" "my-test-instance" {
  ami             = var.ami_type
  instance_type   = var.instance_type
  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"

  # key_name = var.key_pair
  tags = {
    Name = "test-instance"
  }
}